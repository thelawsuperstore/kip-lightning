<?php

namespace TheLawSuperstore\KipLightning;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface ExceptionInterface
 *
 * @package TheLawSuperstore\KipLightning
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
interface ExceptionInterface
{
    /**
     * If this method returns TRUE then this exception will be dispatched. E.g.
     *
     * // Dispatch if this is a 404 - not found response
     * public function dispatchedWhen(ResponseInterface $response)
     * {
     *     return ($response->getStatusCode() == 404);
     * }
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return bool
     */
    public function dispatchedWhen(RequestInterface $request, ResponseInterface $response);
}
