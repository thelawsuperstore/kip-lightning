<?php

namespace TheLawSuperstore\KipLightning;

use GuzzleHttp\Exception\ClientException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;

/**
 * Class Client
 *
 * @package TheLawSuperstore\KipLightning
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class Client
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $auth;

    /**
     * @var ExceptionInterface[]
     */
    protected $exceptions = [];

    /**
     * @var callable[]
     */
    protected $stack = [];

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->config = $resolver->resolve($config);
        $this->stack = HandlerStack::create();
    }

    /**
     * Configure options
     *
     * @param OptionsResolver $resolver
     */
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired([
                'base_uri',
                'affinity_partner_uuid',
                'ip',
            ])
            ->setDefaults([
                'ip' => $_SERVER['REMOTE_ADDR'],
            ]);
    }

    /**
     * Register an exception (pass fully qualified class namespace of exception)
     *
     * @param string $exceptionClass
     * @return $this
     */
    public function registerException($exceptionClass)
    {
        $exception = new $exceptionClass;

        if (! ($exception instanceof ExceptionInterface)) {
            throw new \InvalidArgumentException(sprintf('Exception, "%s", must implement TheLawSuperstore\KipLightning\ExceptionInterface', $exceptionClass));
        }

        if (! ($exception instanceof \Exception)) {
            throw new \InvalidArgumentException(sprintf('Exception, "%s", must extend a standard \Exception', $exceptionClass));
        }

        $this->exceptions[] = $exception;

        return $this;
    }

    /**
     * Register middleware for the HTTP client.
     *
     * @param  callable  $middleware
     * @return $this
     */
    public function registerClientMiddleware(callable $middleware)
    {
        $this->stack->push($middleware);

        return $this;
    }

    /**
     * Register a collection of exceptions
     *
     * @param array $exceptionClasses
     * @return $this
     */
    public function registerExceptions(array $exceptionClasses)
    {
        foreach ($exceptionClasses as $exceptionClass) {
            $this->registerException($exceptionClass);
        }

        return $this;
    }

    /**
     * Set auth parameters (i.e. from login endpoint)
     *
     * @param string $uuid
     * @param string $key
     * @return $this
     */
    public function setAuthentication($uuid, $key)
    {
        $this->auth = [
            'uuid' => $uuid,
            'key' => $key,
        ];

        return $this;
    }

    /**
     * Remove auth params
     *
     * @return $this
     */
    public function removeAuthentication()
    {
        $this->auth = null;

        return $this;
    }

    /**
     * Build WSSE header
     *
     * @param string $uuid
     * @param string $key
     * @return string
     */
    protected function buildWsseHeader($uuid, $key)
    {
        $nonce = base64_encode(substr(md5(uniqid(mt_rand(), true)), 0, 16));
        $created = date('c');
        $digest = base64_encode(sha1(base64_decode($nonce).$created.$key, true));

        return sprintf(
            'UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',
            $uuid,
            $digest,
            $nonce,
            $created
        );
    }

    /**
     * Make a request to the API
     *
     * @param string $method
     * @param string $uri
     * @param null $body
     * @param array|null $headers Override headers
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ExceptionInterface
     * @throws ClientException
     */
    public function request($method, $uri, $body = null, array $headers = null)
    {
        $options = [];
        if ($body) {
            $options['body'] = $body;
        }

        $options['headers'] = [
            'Content-Type' => 'application/json',
            'X-Iris-Affinity-Partner' => $this->config['affinity_partner_uuid'],
            'X-Iris-Remote-Ip' => $this->config['ip'],
        ];

        if (is_array($this->auth)) {
            $options['headers']['Authorization'] = 'WSSE profile="UsernameToken"';
            $options['headers']['X-WSSE'] = $this->buildWsseHeader($this->auth['uuid'], $this->auth['key']);
        }

        if ($headers) {
            $options['headers'] = array_merge($options['headers'], $headers);
        }

        try {
            $client = new GuzzleClient([
                'base_uri' => $this->config['base_uri'],
                'handler' => $this->stack,
            ]);

            $response = $client->request($method, $uri, $options);
        } catch (ClientException $e) {
            foreach ($this->exceptions as $exception) {
                if ($exception->dispatchedWhen($e->getRequest(), $e->getResponse())) {
                    throw $exception;
                }
            }
            throw $e;
        }

        return $response;
    }
}
