Kip Lightning
=============

Simple client for interfacting with our REST API

Example usage:

```
<?php

require __DIR__.'/vendor/autoload.php';

$client = new \TheLawSuperstore\KipLightning\Client([
    'base_uri' => 'http://localhost:8001/app_dev.php/api/v1/',
    'affinity_partner_uuid' => '694d6a8b-7ff8-11e5-98db-080027f2f2d9',
    'ip' => $_SERVER['REMOTE_ADDR'],
]);

// Custom 404 exception type
class MyNotFoundException extends \RuntimeException implements \TheLawSuperstore\KipLightning\ExceptionInterface 
{
    public function dispatchedWhen(\Psr\Http\Message\RequestInterface $request, \Psr\Http\Message\ResponseInterface $response)
    {
        return ($response->getStatusCode() == 404);
    }
}

// Pass class namespace to register exceptions
$client->registerExceptions([
    'MyNotFoundException'
]);

// 1. Login
try {
    $response = $client
        ->request('POST', 'system/security/login', json_encode(['email' => 'tls_customer_01@irisdev.co.uk', 'password' => 'Password1']));
} catch (\Exception $e) {
    exit($e->getMessage());
}

$token = json_decode((string) $response->getBody(), true);

// 2. Get my user profile
try {
    $response = $client
        ->setAuthentication($token['uuid'], $token['key'])
        ->request('GET', 'users/me');
} catch (MyNotFoundException $e) {
    exit('Not found!');
} catch (\Exception $e) {
    exit($e->getMessage());
}

echo (string) $response->getBody();
   
```